import sqlite3
from pip._vendor.distlib.compat import raw_input

def creabasededatos():
    print(":: Creando base de datos")
    conn = sqlite3.connect('Alumnos.db')
    c = conn.cursor()
    # Create table
    c.execute('''CREATE TABLE Alumno(matricula text, nombre text, apaterno text, amaterno text, calif real, email text)''')
    # Save (commit) the changes
    conn.commit()
    # Cerrando conección
    conn.close()
    print("----------------------")
    input("[Enter para volver]")