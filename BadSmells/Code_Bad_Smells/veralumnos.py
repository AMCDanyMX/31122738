import sqlite3
from pip._vendor.distlib.compat import raw_input

def funcionveralumnos():
    print(":: Ver Alumnos")
    conn = sqlite3.connect('Alumnos.db')
    c = conn.cursor()
    c.execute('SELECT * FROM Alumno ')
    # print(c.fetchall())
    for a in c:
        print("\n- [Matricula]: {} [Nombre]: {} [A Paterno]: {} [A Materno]:{} [Calificación]: {} [Email]: {}".format(a[0], a[1], a[2], a[3], a[4], a[5]))
    conn.close()
    print("----------------------")
    input("[Enter para volver]")

