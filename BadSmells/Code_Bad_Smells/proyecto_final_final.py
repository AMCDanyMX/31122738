import sqlite3
from pip._vendor.distlib.compat import raw_input
from veralumnos import funcionveralumnos
from agregaralumno import funcionagregaralumnos
from asignarcalificacion import asignacalificacion
from vercalificacion import vercalificaciones
from crearbasedatos import creabasededatos

def volver():
    print("La opción no es válida.")
    print("----------------------")
    input("[Enter para volver]")


run = True
while run:
    op = input("\n"
               "    :: Menu \n\n"
               "    1.- Ver Alumnos\n"
               "    2.- Agregar Alumno\n"
               "    3.- Asignar calificación\n"
               "    4.- Ver Calificaciones\n"
               "    ---\n"
               "    9.- Crear Base de Datos\n"
               "    0.- Salir\n"
               "    \n Opción: ")
    if op == "1":
       funcionveralumnos()
    elif op == "2":
        funcionagregaralumnos()
    elif op == "3":
       asignacalificacion()
    elif op == "4":
        vercalificaciones()
    elif op == "9":
        creabasededatos()
    elif op == "0":
        run = False
        print("Bye...")
    else:
        volver()
