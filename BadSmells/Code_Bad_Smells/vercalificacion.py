import sqlite3
from pip._vendor.distlib.compat import raw_input

def vercalificaciones():
    print(":: Ver Calificaciones")
    conn = sqlite3.connect('Alumnos.db')
    c = conn.cursor()
    c.execute('SELECT * FROM Alumno ')
    for a in c:
        print("\n- [{} {} {}]: {} ".format(a[1], a[2], a[3], a[4]))
    conn.close()
    print("----------------------")
    input("[Enter para volver]")