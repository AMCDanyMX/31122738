import sqlite3
from pip._vendor.distlib.compat import raw_input

def funcionagregaralumnos():
    print(":: Agregar Alumno")
    conn = sqlite3.connect('Alumnos.db')
    matricula = raw_input("\nMatricula: ")
    nombre = raw_input("\nNombre: ")
    apaterno = raw_input("\nApellido Paterno: ")
    amaterno = raw_input("\nApellido Materno: ")
    calif = raw_input("\nCalificación: ")
    email = raw_input("\nEmail: ")
    c = conn.cursor()
    # Insert a row of data
    c.execute('INSERT INTO Alumno VALUES (?,?,?,?,?,?)', (matricula, nombre, apaterno, amaterno, calif, email))
    # Save (commit) the changes
    conn.commit()
    # Cerrando conección
    conn.close()
    print("Alumno agregado correctamente!")
    print("----------------------")
    input("[Enter para volver]")