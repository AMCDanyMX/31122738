import sqlite3
from pip._vendor.distlib.compat import raw_input

def asignacalificacion():
    print(":: Asignar Calificación")
    conn = sqlite3.connect('Alumnos.db')
    c = conn.cursor()
    # Todo: Solicitar Matricula
    matricula = raw_input("Matricula del Alumno: ")
    # Todo: Verificar que exista
    c.execute('select matricula from Alumno where matricula=?', [matricula])
    if c.fetchone() == None:
        print("La matricula no existe")
        conn.close()
    else:
        # Todo: Actualizar calificación
        calif = input(" - Calificación: ")
        c.execute('update Alumno set calif=? where matricula=?', [calif, matricula])
        conn.commit()
        conn.close()

    print("----------------------")
    input("[Enter para volver]")
